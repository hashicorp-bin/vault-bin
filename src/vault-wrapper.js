#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'vault.exe' : './vault';
const command = resolve(__dirname, '..', 'tools', execName);
const vault = spawn(command, process.argv.slice(2), { cwd: process.cwd() });

vault.stdout.pipe(process.stdout);
vault.stderr.pipe(process.stderr);
vault.on('error', function(err) {
  console.error(`Received an error while executing the Vault binary: ${err}`);
  process.exit(1);
});
